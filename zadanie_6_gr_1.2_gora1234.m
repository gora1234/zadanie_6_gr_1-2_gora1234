path='C:\Users\student\Desktop\jsonlab';
addpath(path);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%dane z danepubliczne.imgw.pl
%katowice_1

url_1=('https://danepubliczne.imgw.pl/api/data/synop/id/12560/format/json');

data_1=loadjson(urlread(url_1));
disp(data_1); %writes all data from the link above 

%below prints the given data as characters, not numbers
fprintf('Pressure : %s\n', data_1.cisnienie);
fprintf('Temperature : %s\n', data_1.temperatura);
fprintf('Speed : %s\n', data_1.predkosc_wiatru);
fprintf('Humidity : %s\n', data_1.wilgotnosc_wzgledna);
fprintf('Hour : %s\n', data_1.godzina_pomiaru);


%change characters to numbers
Pressure_1=str2num(data_1.cisnienie);
Temperature_1=str2num(data_1.temperatura)
Speed_1=str2num(data_1.predkosc_wiatru)
Humidity_1=str2num(data_1.wilgotnosc_wzgledna)
Hour_1=str2num(data_1.godzina_pomiaru)


%from Jacek Pawlyta
% define file name to which will will write the data
fileName_1='datawheather_Katowice.csv';

% build two lines header of the table
header={'Pressure','Temperature','Speed','Humidity','Hour'};
headerUnits = {'hPa', '�C', 'm/s','s/m^3','h'};

% just put some near-real data in the matrix
values = [Pressure_1,Temperature_1,Speed_1,Humidity_1,Hour_1];


% show on the screen how the data looks like
disp(' ');
disp(header);
disp(headerUnits);
disp(values);

% open datafile to write headers in it
% if file existed before it will be overwritten 'w+'

dataFileId=fopen(fileName_1,'w+');

% write down the first line of the header into the datafile
% make CSV file semicollon separated
fprintf(dataFileId,'%s %s %s %s %s %s %s %s %s %s %s\n', '"',header{1},'";"',header{2},'";"',header{3},'";"',header{4},'";"',header{5},'"');

% write down the second line of header into datafile (units)
fprintf(dataFileId,'%s%s%s%s%s%s%s%s%s%s%s\n', '"',headerUnits{1},'";"',headerUnits{2},'";"',headerUnits{3},'";"',headerUnits{4},'";"',headerUnits{5},'"');

% close the datafile so we can append to it some data in other way
fclose(dataFileId);

% write some data to the datafile
dlmwrite(fileName_1,values, '-append','delimiter', ';');
%write some more data to the file (the same values as prevoiusly)
dlmwrite(fileName_1,values, '-append','delimiter', ';');

% say goodbay
disp(['Try to import data from CSV file: ',fileName_1,' to Libre Calc or Excel, keep smiling']);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%czestochowa_2

url_2=('https://danepubliczne.imgw.pl/api/data/synop/id/12550/format/json');

data_2=loadjson(urlread(url_2));
disp(data_2);
fprintf('Pressure : %s\n', data_2.cisnienie);
fprintf('Temperature : %s\n', data_2.temperatura);
fprintf('Speed : %s\n', data_2.predkosc_wiatru);
fprintf('Humidity : %s\n', data_2.wilgotnosc_wzgledna);
fprintf('Hour : %s\n', data_2.godzina_pomiaru);

Pressure_2=str2num(data_2.cisnienie)
Temperature_2=str2num(data_2.temperatura)
Speed_2=str2num(data_2.predkosc_wiatru)
Humidity_2=str2num(data_2.wilgotnosc_wzgledna)
Hour_2=str2num(data_2.godzina_pomiaru)

%from Jacek Pawlyta
% define file name to which will will write the data
fileName_2='datawheather_Czestochowa.csv';

% build two lines header of the table
header={'Pressure','Temperature','Speed','Humidity','Hour'};
headerUnits = {'hPa', '�C', 'm/s','s/m^3','h'};

% just put some near-real data in the matrix
values = [Pressure_2,Temperature_2,Speed_2,Humidity_2,Hour_2];


% show on the screen how the data looks like
disp(' ');
disp(header);
disp(headerUnits);
disp(values);

% open datafile to write headers in it
% if file existed before it will be overwritten 'w+'

dataFileId=fopen(fileName_2,'w+');

% write down the first line of the header into the datafile
% make CSV file semicollon separated
fprintf(dataFileId,'%s %s %s %s %s %s %s %s %s %s %s\n', '"',header{1},'";"',header{2},'";"',header{3},'";"',header{4},'";"',header{5},'"');

% write down the second line of header into datafile (units)
fprintf(dataFileId,'%s%s%s%s%s%s%s%s%s%s%s\n', '"',headerUnits{1},'";"',headerUnits{2},'";"',headerUnits{3},'";"',headerUnits{4},'";"',headerUnits{5},'"');

% close the datafile so we can append to it some data in other way
fclose(dataFileId);

% write some data to the datafile
dlmwrite(fileName_2,values, '-append','delimiter', ';');
%write some more data to the file (the same values as prevoiusly)
dlmwrite(fileName_2,values, '-append','delimiter', ';');

% say goodbay
disp(['Try to import data from CSV file: ',fileName_2,' to Libre Calc or Excel, keep smiling']);









%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Racib�rz_3
url_3=('https://danepubliczne.imgw.pl/api/data/synop/id/12540/format/json');

data_3=loadjson(urlread(url_3));
disp(data_3);
fprintf('Pressure : %s\n', data_3.cisnienie);
fprintf('Temperature : %s\n', data_3.temperatura);
fprintf('Speed : %s\n', data_3.predkosc_wiatru);
fprintf('Humidity : %s\n', data_3.wilgotnosc_wzgledna);
fprintf('Hour : %s\n', data_3.godzina_pomiaru);

Pressure_3=str2num(data_3.cisnienie)
Temperature_3=str2num(data_3.temperatura)
Speed_3=str2num(data_3.predkosc_wiatru)
Humidity_3=str2num(data_3.wilgotnosc_wzgledna)
Hour_3=str2num(data_3.godzina_pomiaru)


%from Jacek Pawlyta
% define file name to which will will write the data
fileName_3='datawheather_Racib�rz.csv';

% build two lines header of the table
header={'Pressure','Temperature','Speed','Humidity','Hour'};
headerUnits = {'hPa', '�C', 'm/s','s/m^3','h'};

% just put some near-real data in the matrix
values = [Pressure_3,Temperature_3,Speed_3,Humidity_3,Hour_3];


% show on the screen how the data looks like
disp(' ');
disp(header);
disp(headerUnits);
disp(values);

% open datafile to write headers in it
% if file existed before it will be overwritten 'w+'

dataFileId=fopen(fileName_3,'w+');

% write down the first line of the header into the datafile
% make CSV file semicollon separated
fprintf(dataFileId,'%s %s %s %s %s %s %s %s %s %s %s\n', '"',header{1},'";"',header{2},'";"',header{3},'";"',header{4},'";"',header{5},'"');

% write down the second line of header into datafile (units)
fprintf(dataFileId,'%s%s%s%s%s%s%s%s%s%s%s\n', '"',headerUnits{1},'";"',headerUnits{2},'";"',headerUnits{3},'";"',headerUnits{4},'";"',headerUnits{5},'"');

% close the datafile so we can append to it some data in other way
fclose(dataFileId);

% write some data to the datafile
dlmwrite(fileName_3,values, '-append','delimiter', ';');
%write some more data to the file (the same values as prevoiusly)
dlmwrite(fileName_3,values, '-append','delimiter', ';');

% say goodbay
disp(['Try to import data from CSV file: ',fileName_3,' to Libre Calc or Excel, keep smiling']);




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%dane z wttr.in gliwice
urw_1=('http://wttr.in/gliwice?format=j1')
data_4=loadjson(urlread(urw_1));
disp(data_4);


fprintf('Pressure : %s\n', data_4.cisnienie);
fprintf('Temperature : %s\n', data_4.temperatura);
fprintf('Speed : %s\n', data_4.predkosc_wiatru);
fprintf('Humidity : %s\n', data_4.wilgotnosc_wzgledna);
fprintf('Hour : %s\n', data_4.godzina_pomiaru);

Pressure_4=str2num(data_4.cisnienie)
Temperature_4=str2num(data_4.temperatura)
Speed_4=str2num(data_4.predkosc_wiatru)
Humidity_4=str2num(data_4.wilgotnosc_wzgledna)
Hour_4=str2num(data_4.godzina_pomiaru)

%from Jacek Pawlyta
% define file name to which will will write the data
fileName_4='datawheather_gliwice_wttr.csv';

% build two lines header of the table
header={'Pressure','Temperature','Speed','Humidity','Hour'};
headerUnits = {'hPa', '�C', 'm/s','s/m^3','h'};

% just put some near-real data in the matrix
values = [Pressure_4,Temperature_4,Speed_4,Humidity_4,Hour_4];


% show on the screen how the data looks like
disp(' ');
disp(header);
disp(headerUnits);
disp(values);

% open datafile to write headers in it
% if file existed before it will be overwritten 'w+'

dataFileId=fopen(fileName_4,'w+');

% write down the first line of the header into the datafile
% make CSV file semicollon separated
fprintf(dataFileId,'%s %s %s %s %s %s %s %s %s %s %s\n', '"',header{1},'";"',header{2},'";"',header{3},'";"',header{4},'";"',header{5},'"');

% write down the second line of header into datafile (units)
fprintf(dataFileId,'%s%s%s%s%s%s%s%s%s%s%s\n', '"',headerUnits{1},'";"',headerUnits{2},'";"',headerUnits{3},'";"',headerUnits{4},'";"',headerUnits{5},'"');

% close the datafile so we can append to it some data in other way
fclose(dataFileId);

% write some data to the datafile
dlmwrite(fileName_4,values, '-append','delimiter', ';');
%write some more data to the file (the same values as prevoiusly)
dlmwrite(fileName_4,values, '-append','delimiter', ';');

% say goodbay
disp(['Try to import data from CSV file: ',fileName_4,' to Libre Calc or Excel, keep smiling']);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%sosnowiec

urw_2=('http://wttr.in/sosnowiec?format=j1')
data_5=loadjson(urlread(urw_2));
disp(data_5);

fprintf('Pressure : %s\n', data_5.cisnienie);
fprintf('Temperature : %s\n', data_5.temperatura);
fprintf('Speed : %s\n', data_5.predkosc_wiatru);
fprintf('Humidity : %s\n', data_5.wilgotnosc_wzgledna);
fprintf('Hour : %s\n', data_5.godzina_pomiaru);

Pressure_5=str2num(data_5.cisnienie)
Temperature_5=str2num(data_5.temperatura)
Speed_5=str2num(data_5.predkosc_wiatru)
Humidity_5=str2num(data_5.wilgotnosc_wzgledna)
Hour_5=str2num(data_5.godzina_pomiaru)

%from Jacek Pawlyta
% define file name to which will will write the data
fileName_5='datawheather_sosnowiec_wttr.csv';

% build two lines header of the table
header={'Pressure','Temperature','Speed','Humidity','Hour'};
headerUnits = {'hPa', '�C', 'm/s','s/m^3','h'};

% just put some near-real data in the matrix
values = [Pressure_5,Temperature_5,Speed_5,Humidity_5,Hour_5];


% show on the screen how the data looks like
disp(' ');
disp(header);
disp(headerUnits);
disp(values);

% open datafile to write headers in it
% if file existed before it will be overwritten 'w+'

dataFileId=fopen(fileName_5,'w+');

% write down the first line of the header into the datafile
% make CSV file semicollon separated
fprintf(dataFileId,'%s %s %s %s %s %s %s %s %s %s %s\n', '"',header{1},'";"',header{2},'";"',header{3},'";"',header{4},'";"',header{5},'"');

% write down the second line of header into datafile (units)
fprintf(dataFileId,'%s%s%s%s%s%s%s%s%s%s%s\n', '"',headerUnits{1},'";"',headerUnits{2},'";"',headerUnits{3},'";"',headerUnits{4},'";"',headerUnits{5},'"');

% close the datafile so we can append to it some data in other way
fclose(dataFileId);

% write some data to the datafile
dlmwrite(fileName_5,values, '-append','delimiter', ';');
%write some more data to the file (the same values as prevoiusly)
dlmwrite(fileName_5,values, '-append','delimiter', ';');

% say goodbay
disp(['Try to import data from CSV file: ',fileName_5,' to Libre Calc or Excel, keep smiling']);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
urw_3=('http://wttr.in/warszawa?format=j1')
data_6=loadjson(urlread(urw_3));
disp(data_6);

fprintf('Pressure : %s\n', data_6.cisnienie);
fprintf('Temperature : %s\n', data_6.temperatura);
fprintf('Speed : %s\n', data_6.predkosc_wiatru);
fprintf('Humidity : %s\n', data_6.wilgotnosc_wzgledna);
fprintf('Hour : %s\n', data_6.godzina_pomiaru);

Pressure_6=str2num(data_6.cisnienie)
Temperature_6=str2num(data_6.temperatura)
Speed_6=str2num(data_6.predkosc_wiatru)
Humidity_6=str2num(data_6.wilgotnosc_wzgledna)
Hour_6=str2num(data_6.godzina_pomiaru)


%from Jacek Pawlyta
% define file name to which will will write the data
fileName_6='datawheather_warszawa_wttr.csv';

% build two lines header of the table
header={'Pressure','Temperature','Speed','Humidity','Hour'};
headerUnits = {'hPa', '�C', 'm/s','s/m^3','h'};

% just put some near-real data in the matrix
values = [Pressure_6,Temperature_6,Speed_,Humidity_6,Hour_6];


% show on the screen how the data looks like
disp(' ');
disp(header);
disp(headerUnits);
disp(values);

% open datafile to write headers in it
% if file existed before it will be overwritten 'w+'

dataFileId=fopen(fileName_6,'w+');

% write down the first line of the header into the datafile
% make CSV file semicollon separated
fprintf(dataFileId,'%s %s %s %s %s %s %s %s %s %s %s\n', '"',header{1},'";"',header{2},'";"',header{3},'";"',header{4},'";"',header{5},'"');

% write down the second line of header into datafile (units)
fprintf(dataFileId,'%s%s%s%s%s%s%s%s%s%s%s\n', '"',headerUnits{1},'";"',headerUnits{2},'";"',headerUnits{3},'";"',headerUnits{4},'";"',headerUnits{5},'"');

% close the datafile so we can append to it some data in other way
fclose(dataFileId);

% write some data to the datafile
dlmwrite(fileName_6,values, '-append','delimiter', ';');
%write some more data to the file (the same values as prevoiusly)
dlmwrite(fileName_6,values, '-append','delimiter', ';');

% say goodbay
disp(['Try to import data from CSV file: ',fileName_6,' to Libre Calc or Excel, keep smiling']);
